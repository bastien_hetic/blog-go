package main

import (
	"fmt"
	"log"
	"net/http"
	"gitlab.com/bastien_hetic/blog-go/config"
	"gitlab.com/bastien_hetic/blog-go/models"
)

func main() {
	config.VarEnv()
	config.GormSql()
	config.Db().AutoMigrate(&models.User{}, &models.Post{}, &models.Comment{})

	//Initialize Router
	router := InitializeRouter()
	
	fmt.Println("Project is running and listening to 127.0.0.1:8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
