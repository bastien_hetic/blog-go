## Installation

First clone the repository
```
git clone https://gitlab.com/bastien_hetic/blog-go.git
```

Download dependencies and build vendor folder
```
go mod vendor
```

Execute this to run project locally
```
go run *.go
```

Build the project with this command
```
go build
```

###### Installation mysql
The project need a MySQL database to work
Follow this sections if you doesn't have an operationnal MySQL installation on Linux (Ubuntu)

Install the package
```bash
sudo apt install mysql-server
```
Start the service
```bash
sudo systemctl start mysql
```
Follow this commands to log and create the database
> Connection with user root without password
```
mysql -u root
CREATE DATABASE db.name;
```

###### Edit the configuration file (.ENV)
You must modify the environment variables to adapt them to your MySQL installation. 

```bash
MYSQL_USERNAME = YOUR_USERNAME  //The username used to connect => **root**
MYSQL_DATABASE = YOUR_DATABASE  //The name of your database => **db.name**
MYSQL_PASSWORD = YOUR_PASSWORD  //The username password, leave empty if necessary
MYSQL_PORT = YOUR_PORT  //The port of your MySQL installation, default is **3306**
MYSQL_HOST = YOUR_HOST  //The address of your MySQL server
```

## Documentation

###### Routes
Routes used for **users**
```bash
GET http://localhost:8080/users     //Route Get Users
POST http://localhost:8080/register     //Route Create User
POST http://localhost:8080/login     //Route Login with token JWT
```
Routes used for **posts**
```bash
GET http://localhost:8080/posts     //Route Get Posts
POST http://localhost:8080/posts     //Route Create Post 
PUT http://localhost:8080/posts/{id}     //Route Update Post
DELETE http://localhost:8080/posts/{id}     //Route Delete Post 
```
Routes used for **comments**
```bash
GET http://localhost:8080/posts/{id}/comments     //Route Get Comments
POST http://localhost:8080/posts/{id}/comments     //Route Create Comment 
PUT http://localhost:8080/posts/{post_id}/comments/{id}     //Route Update Comment
DELETE http://localhost:8080/posts/{post_id}/comments/{id}     //Route Delete Comment 
```

For more details follow this link
[Google Doc API Blog-Go](https://drive.google.com/drive/u/2/folders/1wvQj8f9hNUQsfSRIw7nwUz7lZpvczTgm)

## Project Developed By 
**Bastien DOMART -
Chabha MAHFOUFI -
Filoupatir HANNA -
Tan NGUYEN**
