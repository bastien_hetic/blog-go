package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/bastien_hetic/blog-go/controllers"
)

func InitializeRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	
	//Users
	router.Methods("GET").Path("/users").Name("ViewUsers").HandlerFunc(controllers.ViewUsers)
	router.Methods("POST").Path("/register").Name("Register").HandlerFunc(controllers.Register)
	router.Methods("POST").Path("/login").Name("Login").HandlerFunc(controllers.Login)

	//Posts
	router.Methods("GET").Path("/posts").Name("ViewPosts").HandlerFunc(controllers.ViewPosts)
	router.Methods("POST").Path("/posts").Name("NewPost").HandlerFunc(controllers.NewPost)
	router.Methods("PUT").Path("/posts/{id}").Name("EditPost").HandlerFunc(controllers.EditPost)
	router.Methods("DELETE").Path("/posts/{id}").Name("RemovePost").HandlerFunc(controllers.RemovePost)

	//Comments
	router.Methods("GET").Path("/posts/{id}/comments").Name("ViewPostComments").HandlerFunc(controllers.ViewPostComments)
	router.Methods("POST").Path("/posts/{id}/comments").Name("NewComment").HandlerFunc(controllers.NewComment)
	router.Methods("PUT").Path("/posts/{post_id}/comments/{id}").Name("EditComment").HandlerFunc(controllers.EditComment)
	router.Methods("DELETE").Path("/posts/{post_id}/comments/{id}").Name("RemoveComment").HandlerFunc(controllers.RemoveComment)

	return router
}
