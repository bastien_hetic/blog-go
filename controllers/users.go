package controllers

import (
	"encoding/json"
	"net/http"
	"gitlab.com/bastien_hetic/blog-go/models"
)

func Login(w http.ResponseWriter, r *http.Request) {
	var credentials models.Credentials

	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	//Get the expected password
	var user *models.User
	user, err = models.FindUserByEmail(credentials.Email)

	//If there is no combination of email/password found
	//then we return an "Unauthorized" status
	if user.Password != credentials.Password {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	err = setUserTokenCookie(w, user.Email)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

//Create new User
func Register(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var newUser *models.User
	newUser, err = models.CreateUser(&user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json;charset=UTF")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(newUser)
}

//Select Users
func ViewUsers(w http.ResponseWriter, r *http.Request) {
	users, err := models.FindUsers()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json;charset=UTF")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(users)
}