package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/bastien_hetic/blog-go/models"
)

func NewPost(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		var post models.Post
		err := json.NewDecoder(r.Body).Decode(&post)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var newPost *models.Post
		newPost, err = models.CreatePost(&post)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-type", "application/json;charset=UTF")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(newPost)
	}
}

func ViewPosts(w http.ResponseWriter, r *http.Request) {
	posts, err := models.FindPosts()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json;charset=UTF")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(posts)
}

func EditPost(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var post *models.Post
		post, err = models.FindPostById(id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&post)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var updPost *models.Post
		updPost, err = models.UpdatePost(post)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-type", "application/json;charset=UTF")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(updPost)
	}
}

func RemovePost(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var post *models.Post
		post, err = models.FindPostById(id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = models.DeletePost(post)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}