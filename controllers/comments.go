package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/bastien_hetic/blog-go/models"
)

func NewComment(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		var comment models.Comment
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		
		err = json.NewDecoder(r.Body).Decode(&comment)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		claims, ok := extractClaims(w, r)
		if claims == nil || !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	
		var user *models.User
		user, err = models.FindUserByEmail(claims["email"].(string))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		comment.PostID = uint(id)
		comment.UserID = user.ID

		var newComment *models.Comment
		newComment, err = models.CreateComment(&comment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-type", "application/json;charset=UTF")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(newComment)
	}
}

func ViewPostComments(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	
	post, err := models.FindPostById(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-type", "application/json;charset=UTF")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(post.Comments)
}

func EditComment(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		vars := mux.Vars(r)
		post_id, err := strconv.Atoi(vars["post_id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		
		var id int
		id, err = strconv.Atoi(vars["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var comment *models.Comment
		comment, err = models.FindCommentById(id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&comment)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		claims, ok := extractClaims(w, r)
		if claims == nil || !ok {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	
		var user *models.User
		user, err = models.FindUserByEmail(claims["email"].(string))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		comment.PostID = uint(post_id)
		comment.UserID = user.ID

		var updComment *models.Comment
		updComment, err = models.UpdateComment(comment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-type", "application/json;charset=UTF")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(updComment)
	}
}

func RemoveComment(w http.ResponseWriter, r *http.Request) {
	if isAuthenticated(w, r) {
		vars := mux.Vars(r)
		id, err := strconv.Atoi(vars["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var comment *models.Comment
		comment, err = models.FindCommentById(id)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		err = models.DeleteComment(comment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}