package controllers

import (
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/bastien_hetic/blog-go/config"
	"gitlab.com/bastien_hetic/blog-go/models"
)

var jwtKey = []byte(config.AppConfig()["JWT_KEY"])

func setUserTokenCookie(w http.ResponseWriter, email string) error {
	expirationTime := time.Now().Add(60 * time.Minute)
	claims := &models.Claims{
		Email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err == nil {
		http.SetCookie(w, &http.Cookie{
			Name: "token",
			Value: tokenString,
			Expires: expirationTime,
		})
	}
	
	return err
}

func isAuthenticated(w http.ResponseWriter, r *http.Request) bool {
	c, err := r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			return false
		}

		w.WriteHeader(http.StatusBadRequest)
		return false
	}

	tokenString := c.Value
	claims := &models.Claims{}

	token, err := jwt.ParseWithClaims(tokenString, claims, func(tkn *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			return false
		}
		w.WriteHeader(http.StatusBadRequest)
		return false
	}
	if !token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return false
	}

	return true
}

func extractClaims(w http.ResponseWriter, r *http.Request) (jwt.MapClaims, bool) {
	c, err := r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			return nil, false
		}

		w.WriteHeader(http.StatusBadRequest)
		return nil, false
	}

	tokenString := c.Value
	var token *jwt.Token
	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if (err != nil) {
		return nil, false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, true
	} else {
		return nil, false
	}
}
