package models

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"gitlab.com/bastien_hetic/blog-go/config"
)

//Models
type User struct {
	gorm.Model
	Email    string `gorm:"unique_index:user_email_index"`
	Password string `json:"password"`
	Username string `json:"username"`
	Posts []Post
	Comments []Comment
}

type Users []User

type Claims struct {
	Email string `json:"email"`
	jwt.StandardClaims
}

type Credentials struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

func CreateUser(u *User) (*User, error) {
	result := config.Db().Create(&u)
	return u, result.Error
}

func FindUserByEmail(email string) (*User, error) {
	var user User

	err := config.Db().Where("email = ?", email).First(&user).Error
	return &user, err
}

func FindUsers() (*Users, error) {
	var users Users

	err := config.Db().Find(&users).Error
	return &users, err
}