package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/bastien_hetic/blog-go/config"
)

type Post struct {
	gorm.Model
	UserID uint `json:"user_id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Comments []Comment
}

type Posts []Post

func CreatePost(p *Post) (*Post, error) {
	result := config.Db().Create(&p)
	return p, result.Error
}

func FindPostById(id int) (*Post, error) {
	var post Post

	err := config.Db().Preload("Comments").First(&post, id).Error
	return &post, err
}

func FindPosts() (*Posts, error) {
	var posts Posts

	err := config.Db().Find(&posts).Error
	return &posts, err
}

func UpdatePost(p *Post) (*Post, error){
	result := config.Db().Save(&p)
	return p, result.Error
}

func DeletePost(p *Post) (error){
	result := config.Db().Delete(&p)
	return result.Error
}
