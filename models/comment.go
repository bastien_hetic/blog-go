package models

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/bastien_hetic/blog-go/config"
)

type Comment struct {
	gorm.Model
	UserID uint `json:"user_id"`
	PostID uint `json:"post_id"`
	Text string `json:"text"`
}

func FindCommentById(id int) (*Comment, error) {
	var comment Comment

	err := config.Db().First(&comment, id).Error
	return &comment, err
}

func CreateComment(c *Comment) (*Comment, error) {
	result := config.Db().Create(&c)
	return c, result.Error
}

func UpdateComment(c *Comment) (*Comment, error) {
	result := config.Db().Save(&c)
	return c, result.Error
}

func DeleteComment(c *Comment) (error){
	result := config.Db().Delete(&c)
	return result.Error
}