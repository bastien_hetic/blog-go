package config

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var db *gorm.DB
var appConfig map[string]string
var mysqlCredentials string

func VarEnv() {
	var err error
	appConfig, err = godotenv.Read()

	if err != nil {
		fmt.Println("Error Loading .env file")
	}
}

func GormSql() {
	//user:mdp@tcp(localhost:3306)/db_Name?charset=utf8mb4&parseTime=True&loc=Local
	mysqlCredentials = fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		appConfig["MYSQL_USERNAME"],
		appConfig["MYSQL_PASSWORD"],
		appConfig["MYSQL_HOST"],
		appConfig["MYSQL_PORT"],
		appConfig["MYSQL_DATABASE"],
	)
  
	//Connexion MySQL
	var err error
	db, err = gorm.Open("mysql", mysqlCredentials)

	if err != nil {
		fmt.Println("db mySQL is not connected")
		fmt.Println(err.Error())
	}

	/*defer db.Close()*/
}

//Getter for db var
func Db() *gorm.DB {
	return db
}

//Getter for ENV vars
func AppConfig() map[string]string {
	return appConfig
}