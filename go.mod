module gitlab.com/bastien_hetic/blog-go

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.5 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
)
